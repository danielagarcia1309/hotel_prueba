#Author: danielagarcia1309@gmail.com
@hotel
Feature: Automatización Hotels
  COMO automation tester
  QUIERO automatizar la pagina de Hotels
  PARA que se realice automatico y no manual

  @pagina1
  Scenario Outline: Validar hoteles sin colocar fecha
    Given Navego a la url "https://js.devexpress.com/Demos/DXHotels/#home"
		And Ingreso a Hotel
		And espero 1 segundos
		And verifico que <Elemento> sea visible
		And ingreso a la opción de <Menu> y <Submenu>
		And Doy click en "Check In Calendario"
		And Doy click en "Check In Fecha"
		And espero 3 segundos
		And Doy click en "Search"
		And verifico que <Elemento2> sea visible
		And comparar valores
		And verifico que <Elemento3> sea visible
		And validar suma		
		

		Examples: 
      | Elemento       | Menu       | Submenu           | Elemento2 | Elemento3 | 
      | "Hotel Search" | "Btn Menu" | "Btn Menu Ciudad" |	"result"  |	 "total"  |
      
 @pagina2
  Scenario Outline: Validar hoteles colocando fecha
    Given Navego a la url "https://js.devexpress.com/Demos/DXHotels/#home"
		And Ingreso a Hotel
		And espero 1 segundos
		And verifico que <Elemento> sea visible
		And ingreso a la opción de <Menu> y <Submenu>
		And en el campo de texto "Check In Input" escribo <fecha>
		And Doy click en "adulto"
		And espero 3 segundos
		And Doy click en "Search"
		And verifico que <Elemento2> sea visible
		And comparar valores
		And verifico que <Elemento3> sea visible
		And validar suma		
		
		
		
		Examples: 
      | Elemento       | Menu       | Submenu           | fecha        | Elemento2  | Elemento3 | 
      | "Hotel Search" | "Btn Menu" | "Btn Menu Ciudad" | "03/28/2021" |	"result"  |	 "total"  |