package automationTesting.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import automationTesting.objects.GoogleObjects;
import automationTesting.utilities.ActionsUtil;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://js.devexpress.com/Demos/DXHotels/#home")
public class GooglePage extends BasePage{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GooglePage.class);
	
	public GooglePage() {
		if (ActionsUtil.objetosIsEmpty()) {
			LOGGER.info("Inicialización de objetos");
			new GoogleObjects();
		}
	}

	public void escribir(String objeto, String texto) {
		sharedObjet(objeto);
		ActionsUtil.setTextField(getDriver(), getObjetoToAction(), texto);
	}

	public void click(String objeto) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		sharedObjet(objeto);
		wait.until(ExpectedConditions.visibilityOfElementLocated(getObjetoToAction()));
		ActionsUtil.clic(getDriver(),  getObjetoToAction());
		
	}

	public void contiene(String objeto, String texto) {
		sharedObjet(objeto);
		ActionsUtil.containsText(getDriver(), getObjetoToAction(), texto);
	}
	
	public void verificarVisible(String elemento) {
		sharedObjet(elemento);
		ActionsUtil.verificarVisible(getDriver(), getObjetoToAction());

	}
	
	public void ingresosubmenu(String string, String string2) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		sharedObjet(string);
		wait.until(ExpectedConditions.visibilityOfElementLocated(getObjetoToAction()));
		ActionsUtil.clic(getDriver(), getObjetoToAction());

		sharedObjet(string2);
		wait.until(ExpectedConditions.visibilityOfElementLocated(getObjetoToAction()));
		ActionsUtil.clic(getDriver(), getObjetoToAction());

	}
	
	public void escribirTexto(String campo, String texto) {
		sharedObjet(campo);
		ActionsUtil.setTextField(getDriver(), getObjetoToAction(), texto);

	}
	
	public void esperaSegundos(Integer tiempo) {
		ActionsUtil.sleepSeconds(tiempo);

	}
	
	public void valoresPrecios() {
		ActionsUtil.validarPrecioBajo(getDriver());
	}
	public void valoresPrecios(String campo) {
		sharedObjet(campo);
		WebElement lowerPrice=ActionsUtil.getElementWithMinValue(getDriver(), objetoToAction);
		sharedObjet("Book It Relativo");
		ActionsUtil.getElementRelative(lowerPrice,objetoToAction).click();
	}
	
	
	public void validar_suma() {
		ActionsUtil.validar_suma(getDriver(), objetoToAction, getTitle());
	}
	
	public void validar_suma(String campo) {
		sharedObjet(campo);
		ActionsUtil.getElementWithMinValue(getDriver(), objetoToAction);
	}
	
}
