package automationTesting.definitions;

import automationTesting.pages.GooglePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class GoogleStepDefinitions {
	GooglePage google;
	
	@Given("Navego a la url {string}")
	public void navego_a_la_url(String pageUrl) {
		if(pageUrl.toLowerCase().contains("google")) {
			google.open();
		}
	}

	@Given("Ingreso a Hotel")
	public void ingreso_al_E_Commerce_Claro() {
		google.open();
	}

	@When("Ingreso {string} en el campo {string}")
	public void ingreso_en_el_campo(String texto, String campo) {
		google.escribir(campo,texto);
	}

	@When("Doy click en {string}")
	public void doy_click_en(String campo) {
		google.click(campo);
	}

	@Then("El campo {string} contiene {string}")
	public void el_campo_contiene(String campo, String texto) {
		google.contiene(campo,texto);
	}
	
	@And("verifico que {string} sea visible")
	public void verifico_que_Elemento_sea_visible(String elemento) {
		google.verificarVisible(elemento);
	}
	
	@And("ingreso a la opción de {string} y {string}")
	public void ingreso_a_la_opción_de_y(String string, String string2) {
		google.ingresosubmenu(string, string2);
	}

	@When("en el campo de texto {string} escribo {string}")
	public void en_el_campo_de_texto_escribo(String campo, String texto) {
		google.escribirTexto(campo, texto);
	}
	
	@And("espero {int} segundos")
	public void espero_segundos(Integer tiempo) {
		google.esperaSegundos(tiempo);
	}
	
	@And ("comparar valores")
	public void validar_valores() {
		google.valoresPrecios("Precios");
	}
	
	@And ("validar suma")
	public void validar_suma() {
		google.validar_suma("Precios");
	}

}
