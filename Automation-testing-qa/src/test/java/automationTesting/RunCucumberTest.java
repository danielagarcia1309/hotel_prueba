package automationTesting;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		plugin = { "pretty" }, 
		glue = { "automationTesting" }, 
		features = "src/test/resources/features/hotels.feature"

)
public class RunCucumberTest {
}
