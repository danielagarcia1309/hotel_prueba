package automationTesting.objects;

import org.openqa.selenium.By;

import automationTesting.utilities.ActionsUtil;

public class GoogleObjects {
	public GoogleObjects() {

		ActionsUtil.objetosPut("hotelsearch", By.xpath("//p[contains(text(),'HOTEL SEARCH')]"));
		ActionsUtil.objetosPut("btnmenu", By.xpath("//*[@class='dx-dropdowneditor-icon']"));
		ActionsUtil.objetosPut("btnmenuciudad", By.xpath("//div[@class='dx-item-content dx-list-item-content']"));
		ActionsUtil.objetosPut("checkout", By.id("dx_dx-6afc70b2-d302-83ce-b373-c2188c536c4b_checkOut"));
		ActionsUtil.objetosPut("search", By.xpath("//div[@class='search main-color white-text dx-button dx-button-normal dx-button-mode-contained dx-widget dx-button-has-text']"));
	
		ActionsUtil.objetosPut("checkininput", 		By.xpath("//label[contains(.,'Check In')]/..//input[@class='dx-texteditor-input']"));
		ActionsUtil.objetosPut("checkincalendario", By.xpath("//label[contains(.,'Check In')]/..//input[@class='dx-dropdowneditor-icon']"));
		ActionsUtil.objetosPut("checkinfecha", 		By.xpath("//td[@aria-label='Wednesday, March 31, 2021']"));
		
		ActionsUtil.objetosPut("checkoutinput", 		By.xpath("//label[contains(.,'Check Out')]/..//input[@class='dx-texteditor-input']"));
		ActionsUtil.objetosPut("checkoutcalendario", 	By.xpath("//label[contains(.,'Check Out')]/..//input[@class='dx-dropdowneditor-icon']"));
		ActionsUtil.objetosPut("checkoutfecha", 		By.xpath("//td[@aria-label='Wednesday, March 31, 2021']"));
		
		ActionsUtil.objetosPut("result", By.xpath("//p[contains(text(),'RESULTS')]"));
		ActionsUtil.objetosPut("adulto", By.xpath("//span[contains(text(),'Adults')]"));
		ActionsUtil.objetosPut("precios", By.className("rate-number"));
		ActionsUtil.objetosPut("bookitrelativo", By.xpath("./../..//span"));
		
		

	}

}
